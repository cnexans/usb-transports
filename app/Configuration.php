<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    //

    public static function getValueByKey( $key )
    {
    	return self::where('key', $key)->first()->value;
    }
}
