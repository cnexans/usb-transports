<?php


namespace App\Logicals;


use \GuzzleHttp\Client;

class USBAuthentication
{
	private $service = null;
	private $ticket  = null;

	public function __construct($config)
	{
		if ( isset($config['ticket']) )
			$this->ticket = $config['ticket'];

		if ( isset($config['service']) )
			$this->service = urlencode($config['service']);
	}

	public function redirection($option = null)
	{

		$base = 'https://secure.dst.usb.ve/login?service=[service]';

		$base = str_replace('[service]', $this->service, $base);

		if ($option === 'go')
			return redirect($base);


		return $base;
	}

	public function validate()
	{
		$url = 'https://secure.dst.usb.ve/validate?ticket=[ticket]&service=[service]';


		$url = str_replace('[service]', $this->service, $url);
		$url = str_replace( '[ticket]', $this->ticket,  $url);


		$client = new Client();

		$response = $client->request('GET', $url, [
		    'verify' => false
		]);

		$parse = explode(chr(10), $response->getBody());

		$user = new \stdClass();

		if ( $parse[0] == 'yes' )
		{

			$user->loginSuccess = true;
			$user->carnet       = trim($parse[1]);
		}
		else
		{
			$user->loginSuccess = false;
			$user->carnet       = false;
		}

		return $user;
	}



}
