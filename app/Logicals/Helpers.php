<?php


namespace App\Logicals;

class Helpers
{
	public static function makeCallout($title, $message = null)
	{

		$data['title'] = $title;


		if ( !is_null($message) )
			$data['message'] = $message;


		return ['callout' => $data];

	}

}
