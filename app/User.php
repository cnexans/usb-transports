<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


use Carbon\Carbon as Carbon;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    private static $profileIntervalKey = 'user_profile_edit_days_interval';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['short_name', 'full_name', 'email', 'password', 'carnet'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public static function findByCarnet( $carnet )
    {
        $found = self::where('carnet', $carnet)->first();

        if ( $found == null )
            return false;

        else
            return $found;
    }

    public static function createByCarnet($carnet)
    {

        //$carnet = trim($carnet);

        $user = self::create([
            'carnet' => $carnet,
            'email'  => $carnet . '@usb.ve'
        ]);

        return $user;
    }


    public function updateProfile($data)
    {

        $this->profile_updated_at = Carbon::now();


        $this->full_name  = $data['full_name'];

        $this->short_name = isset($data['short_name']) ? 
            $data['full_name']  : 
            $data['short_name'] ;

        return $this->save();
    }

    public function canUpdateProfile()
    {

        if ( is_null($this->profile_updated_at) )
            return true;

        $daysInterval = self::profileInterval();

        $canUpdateAt  = Carbon::parse( $this->profile_updated_at )->addDays( $daysInterval );

        if ( Carbon::now()->diffInDays( $canUpdateAt, false ) <= 0 )
            return true;

        return false;
    }

    public static function profileInterval()
    {
        return Configuration::getValueByKey( self::$profileIntervalKey );
    }

    public function checkRole($open)
    {
        return in_array($this->role, $open);
    }

    public function canAdmin()
    {
        return $this->checkRole(['admin', 'superadmin', 'developer']);
    }
    public function canSuperAdmin()
    {
        return $this->checkRole(['superadmin', 'developer']);
    }
    public function canDeveloper()
    {
        return $this->checkRole(['developer']);
    }
}
