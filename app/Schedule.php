<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;
use App\ListName;

class Schedule extends Model
{
    protected $fillable = ['name', 'time', 'opens', 'closes'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'list_names')->withPivot('created_at');
    }

    public static function allWithNames()
    {
        $schedules = self::all();

        $schedules = $schedules->map(function($schedule)
        {
            $r            = new \stdClass();
            $r->id        = $schedule->id;
            $r->name      = $schedule->name;
            $r->opens     = Carbon::parse($schedule->opens);
            $r->closes    = Carbon::parse($schedule->closes);
            $r->time      = Carbon::parse($schedule->time);
            $r->available = Carbon::now()->between($r->opens, $r->closes);
            $r->list   = $schedule->users()->get();
            $r->list = $r->list->map(function($user)
            {
                $s = new \stdClass();
                $s->carnet = $user->carnet;
                $s->time   = $user->pivot->created_at;
                $s->name   = $user->short_name;
                return $s;
            })->reject(function ($name) {
                return !$name->time->isToday();
            })->sortBy(function ($user) {
                return $user->time->timestamp;
            });


            return $r;
        });

        return $schedules;
    }

    public static function findWithNames($id)
    {
    	$schedule = self::find($id);
    	if (!$schedule)
    		return null;

        $r            = new \stdClass();
        $r->id        = $schedule->id;
        $r->name      = $schedule->name;
        $r->opens     = Carbon::parse($schedule->opens);
        $r->closes    = Carbon::parse($schedule->closes);
        $r->time      = Carbon::parse($schedule->time);
        $r->available = Carbon::now()->between($r->opens, $r->closes);
        $r->list      = $schedule->users()->get();
        $r->list      = $r->list->map(function($user)
        {
            $s = new \stdClass();
            $s->carnet = $user->carnet;
            $s->time   = $user->pivot->created_at;
            $s->name   = $user->short_name;
            return $s;
        })->reject(function ($name) {
            return !$name->time->isToday();
        })->sortBy(function ($user) {
            return $user->time->timestamp;
        });


        return $r;
    }

    public function write($user)
    {
        $try = ListName::where([
            'user_id'     => $user,
            'schedule_id' => $this->id
        ])->whereDay('created_at', '=', date('d'))
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->get();

        if ( $try->isEmpty() ):
            $r = new ListName();
            $r->schedule_id = $this->id;
            $r->user_id = $user;
            $r->save();
        endif;

    }

    public static function stransformTime($t)
    {
        return Carbon::parse($t)->format('h:i A');
    }

    public function transformTime($t)
    {
        return self::stransformTime($t);
    }

    public function editableTime($t)
    {
        return Carbon::parse($t)->format('H:i');
    }
}
