<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Schedule;
use App\Logicals\Helpers as Helpers;

use App\User;
use Hash;
use Auth;
use Validator;

use \Lang;

class SchedulesController extends Controller
{
    public function write($id)
    {
        $schedule = Schedule::find($id);
        if ( $schedule === null )
            return Redirect::back();

        $schedule->write(Auth()->User()->id);
        $schedule = Schedule::findWithNames($id);
        $callout = Helpers::makeCallout(
            Lang::get('messages.list.ready.title'),
            Lang::get('messages.list.ready.message')
        );
        if (!$schedule)
            return redirect('/');
        return view('list', $callout)->with('schedule', $schedule);
    }

    public function configure()
    {
        $schedules = Schedule::all();
        return view('schedules/configure')->with('schedules', $schedules);
    }

    public function edit($id)
    {
        $schedule = Schedule::find($id);
        return view('schedules/edit')->with('schedule', $schedule)->with('id', $id);
    }

    public function create()
    {
        return view('schedules/create');
    }

    public function update($id, Request $request)
    {
        $schedule = Schedule::find($id);
        $schedule->name = $request->input('name');
        $schedule->time = $request->input('time').':00';
        $schedule->opens = $request->input('opens').':00';
        $schedule->closes = $request->input('closes').':00';
        $schedule->save();



        $callout = Helpers::makeCallout(
            Lang::get('messages.admin.schedules-update.title'),
            Lang::get('messages.admin.schedules-update.message')
        );
        return view('schedules/edit', $callout)->with('schedule', $schedule)->with('id', $id);
    }

    public function store(Request $request)
    {


        $validate = Validator::make( $request->all(), [
            'name'   => 'required',
            'opens'  => 'required',
            'closes' => 'required',
            'time'   => 'required'
        ]);

        $validate->setAttributeNames([
            'name'   => Lang::get('messages.schedule.key.name'),
            'opens'  => Lang::get('messages.schedule.key.opens'),
            'closes' => Lang::get('messages.schedule.key.closes'),
            'time'   => Lang::get('messages.schedule.key.time')
        ]);

        if ( $validate->fails() )
        {

            $callout = Helpers::makeCallout (
                Lang::get('messages.titles.error'),
                $validate->errors()->first()
            );

            return view('schedule/create', $callout);
        }


        $schedule = new Schedule();
        $schedule->name = $request->input('name');
        $schedule->time = $request->input('time').':00';
        $schedule->opens = $request->input('opens').':00';
        $schedule->closes = $request->input('closes').':00';
        $schedule->save();



        $callout = Helpers::makeCallout(
            Lang::get('messages.admin.schedules-update.title'),
            Lang::get('messages.admin.schedules-update.message')
        );

        $schedules = Schedule::all();
        return view('schedules/configure', $callout)->with('schedules', $schedules);
    }
}
