<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Schedule as Schedule;


class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::allWithNames();
        return view('index')->with('schedules', $schedules);
    }

    public function scheduleNames($id)
    {
        $schedule = Schedule::findWithNames($id);

        if (!$schedule)
            return redirect('/');

        return view('list')->with('schedule', $schedule);
    }

}
