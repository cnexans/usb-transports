<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Logicals\USBAuthentication as USBId;
use App\Logicals\Helpers as Helpers;

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\User;
use Hash;
use Auth;
use Validator;

use \Lang;

class UsersController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    private $casService;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->casService = url('/users/login/cas/success');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function casLogin()
    {

        $cas = new UsbId([
            'service' => $this->casService
        ]);

        return $cas->redirection('go');
    }

    /**
     * Process the casRedirection
     *
     * @return \Illuminate\Http\Response
     */
    public function casSuccess(Request $request)
    {
        if ( !$request->has('ticket') )
        {
            return redirect('/errors/403');
        }

        $cas = new UsbId([
            'ticket'  => $request->ticket,
            'service' => $this->casService
        ]);

        $casUser = $cas->validate();


        // returns success = false if there was an error authenticating
        if ( !$casUser->loginSuccess )
        {
            return redirect('/errors/403');
        }


        // Returns false if the user is not found
        $user = User::findByCarnet($casUser->carnet);


        if ( $user === false )
        {
            // returns the user created
            $user = User::createByCarnet( $casUser->carnet );

            //Login the user by the carnet
            Auth::login( $user, true );

            return redirect('/users/profile');

        }

        //Login the user by the carnet
        Auth::login( $user, true );

        return redirect('/');
    }



    /**
     *
     * Logout a user
     *
     */
    
    public function getLogout()
    {

        //delete the user from the session
        Auth::logout();


        return redirect('/');
    }


    /*=======================================================
    =            Provides login and edit profile            =
    =======================================================*/
    public function getNativeLogin()
    {
        return view('auth.login');
    }


    public function postNativeLogin(Request $request)
    {
        $email    = $request->input('email', '');
        $password = $request->input('password', '');

        if ( Auth::attempt(['email' => $email, 'password' => $password]) ) 
        {
            // Authentication passed...
            return redirect('/');
        }
        if ( Auth::attempt(['alt_email' => $email, 'password' => $password]) ) 
        {
            // Authentication passed...
            return redirect('/');
        }

        $callout = Helpers::makeCallout (
            Lang::get('messages.auth.bad_credentials.title'),
            Lang::get('messages.auth.bad_credentials.message')
        );
        return view('auth.login', $callout);


    }

    public function getProfile()
    {
        return view('users.profile');
    }
    
    
    /*=====  End of Provides login and edit profile  ======*/
    
    /**
     *
     * Process accessKey
     *
     */
    

    private function editAccessKey(Request $request)
    {
        $user = Auth()->User();

        $validate = Validator::make( $request->all(), [
            'alt_email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6|max:255'
        ]);

        $validate->setAttributeNames([
            'full_name'  => Lang::get('messages.user_access_key.friendly_names.alt_email'),
            'short_name' => Lang::get('messages.user_access_key.friendly_names.short_name')
        ]);

        if ( $validate->fails() )
        {

            $callout = Helpers::makeCallout (
                Lang::get('messages.titles.error'),
                $validate->errors()->first()
            );
        }
        else
        {

            $user->alt_email = $request->alt_email;
            $user->password  = Hash::make($request->password);

            $user->save();

            $callout = Helpers::makeCallout (
                Lang::get('messages.titles.success'),
                Lang::get('messages.user_profile.success')
            );

        }

        return view('users.profile', $callout);
    }




    /**
     *
     * Process the edit profile
     *
     */
    
    private function editUserProfile(Request $request)
    {
        $user = Auth()->User();

        if ( !$user->canUpdateProfile() )
        {

            $callout = Helpers::makeCallout (
                Lang::get('messages.titles.error'),
                Lang::get('messages.user_profile.edit_denied')
            );

            return view('users.profile', $callout);
        }

        $validate = Validator::make( $request->all(), [
            'full_name'  => 'required|min:4|max:255',
            'short_name' => 'min:4|max:255|unique:users'
        ]);


        $validate->setAttributeNames([
            'full_name'  => Lang::get('messages.user_profile.friendly_names.full_name'),
            'short_name' => Lang::get('messages.user_profile.friendly_names.short_name')
        ]);

        if ( $validate->fails() )
        {

            $callout = Helpers::makeCallout (
                Lang::get('messages.titles.error'),
                $validate->errors()->first()
            );

        }
        else
        {

            $user->updateProfile( $request->all() );

            $callout = Helpers::makeCallout (
                Lang::get('messages.titles.success'),
                Lang::get('messages.user_access_key.success')
            );
        }

        return view('users.profile', $callout);
    }




    /**
     *
     * Unique HTTP request for acces-key and profile
     *
     */
    
    public function postProfile(Request $request)
    {
        $target = $request->input('target', 'unavailable');

        if ( $target == 'access-key' )
        {
            return $this->editAccessKey($request);
        }


        else if ( $target == 'edit-profile' )
        {
            return $this->editUserProfile($request);
        }

        $callout = Helpers::makeCallout(
            Lang::get('messages.titles.error'),
            Lang::get('messages.general.unknown')
        );

        return view('users.profile', $callout);
    }


}
