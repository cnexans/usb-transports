<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'GeneralController@index');


/*================================
=            Auth URLs            =
================================*/

Route::get('/users/login/cas'        , 'UsersController@casLogin'      )->middleware('guest');
Route::get('/users/login/cas/success', 'UsersController@casSuccess'    )->middleware('guest');
Route::get('/users/logout'           , 'UsersController@getLogout'     )->middleware('auth');
Route::get('/users/login'            , 'UsersController@getNativeLogin')->middleware('guest');
Route::post('/users/login'           , 'UsersController@postNativeLogin')->middleware('guest');


/*=====  End of Auth URL  ======*/

/*==================================
=            Users URLs            =
==================================*/

Route::get ('/users/profile',  'UsersController@getProfile')->middleware('auth');
Route::post('/users/profile', 'UsersController@postProfile')->middleware('auth');

/*=====  End of Users URLs  ======*/

/*======================================
=            Schedules URLs            =
======================================*/

Route::get('/schedules/{id}/', 'GeneralController@scheduleNames');
Route::any('/schedules/{id}/write', 'SchedulesController@write')->middleware('auth');
Route::any('/schedule/configure', 'SchedulesController@configure')->middleware('auth')->middleware('admin');
Route::any('/schedules/{id}/edit', 'SchedulesController@edit')->middleware('auth')->middleware('admin');
Route::post('/schedules/{id}/update', 'SchedulesController@update')->middleware('auth')->middleware('admin');
Route::any('/schedules/{id}/delete', 'SchedulesController@delete')->middleware('auth')->middleware('admin');
Route::get('/schedule/create/', 'SchedulesController@create')->middleware('auth')->middleware('admin');
Route::post('/schedule/store/', 'SchedulesController@store')->middleware('auth')->middleware('admin');


/*=====  End of Schedules URLs  ======*/





Route::get('/errors/403', function() {
	return view('errors.403');
});