<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListName extends Model
{
    protected $fillable = ['schedule_id', 'user_id'];
}
