## USBLosAltos *web app*

This web application is being done to help students at the Simon Bolivar University using college transport efficiently. The App
manages 5 kinds of users: user, admin, super admin and developer. The goal is to create bus schedules, the users can put their names in each schedule list. All users must use the official CAS authentication of the university.

### Contributing

You are free to fork and comment, I'd thank you very much.

### Security Vulnerabilities

If you discover a security vulnerability within this repository send and email to me@cnexans.com

### License

This software is under MIT Licence.
