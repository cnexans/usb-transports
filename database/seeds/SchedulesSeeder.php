<?php

use Illuminate\Database\Seeder;

class SchedulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('schedules')->insert([
			'name'   => 'Los Teques',
			'time'   => '14:30:00',
			'opens'   => '14:00:00',
			'closes' => '15:00:00'
        ]);

		DB::table('schedules')->insert([
			'name'   => 'Los Teques',
			'time'   => '15:30:00',
			'opens'   => '15:00:00',
			'closes' => '16:00:00'
        ]);

		DB::table('schedules')->insert([
			'name'   => 'San Antonio',
			'time'   => '15:30:00',
			'opens'   => '15:00:00',
			'closes' => '16:00:00'
        ]);

		DB::table('schedules')->insert([
			'name'   => 'San Antonio',
			'time'   => '14:30:00',
			'opens'   => '14:00:00',
			'closes' => '15:00:00'
        ]);

    }
}
