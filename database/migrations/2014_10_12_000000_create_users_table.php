<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('carnet')->unique();  // required to create user: identity in the university
            $table->string('full_name')->nullable();  // name for reports and list
            $table->string('short_name')->nullable();  // name for the list
            $table->string('email')->nullable();  // institutional email: carnet@usb.ve
            $table->string('alt_email')->nullable(); //used for in-app login
            $table->string('role')->default('user'); //the others are admin and super admin
            /*=============================================
            =            Section comment block            =
            =============================================
            
            Roles ===== user, admin, superadmin, developer

            User ==== solo se anota
            admin ==== puede cambiar horarios de listas, borrar, agregar
            superadmin === admin + revocar y dar permisos de admin a usuarios
            developer ==== admin + puede dar y revocar permisos de admin, superadmin + puede registrar usuarios
            
            /*=====  End of Section comment block  ======*/
            
            $table->string('password', 60)->nullable(); // used for in-app login
            $table->rememberToken();
            $table->timestamps();

            $table->timestamp('profile_updated_at')->nullable(); // ultima vez que el ususario actualizo su perfil
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_names');
        Schema::dropIfExists('schedules');
        Schema::drop('users');
    }
}
