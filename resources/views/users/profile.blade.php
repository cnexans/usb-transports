@extends('layouts.master')

@section('title', 'Home')


@section('content-header')
<h1>
	Configuracion de mi usuario
</h1>
@endsection




@section('content')
<div class="col-lg-6 col-md-12">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title">Mi perfil en el sistema</h3>
		</div><!-- /.box-header -->
		<!-- form start -->
		<div class="col-md-12">
			<p>
				Para poder anotarte en las listas del transporte, primero debes dar los datos de tu perfil, estos solo
				se pueden cambiar cada {{ \App\User::profileInterval() }} dias
			</p>
			<p>El nombre completo te identificara para hacer reportes ante la 
				direccion de servicios. El nombre de pila es para la lista, es unico entre todos los usuarios y opcional.
			</p>
		</div>
		<form class="form-horizontal" method="post" action="{{ url('/users/profile') }}">
			<input type="hidden" value="edit-profile" name="target">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<div class="box-body">
				<div class="form-group">
					<label for="full_name" class="col-sm-12 control-label">Nombre completo</label>
					<div class="col-sm-12">
						<input type="text" class="form-control gray-border" 
						name="full_name" 
						value="{{{ isset(Auth::user()->full_name) ? Auth::user()->full_name : '' }}}" 
						placeholder="Nombre" required>
					</div>
				</div>
				<div class="form-group">
					<label for="short_name" class="col-sm-12 control-label">Nombre de pila</label>
					<div class="col-sm-12">
						<input type="text" class="form-control gray-border" 
						value="{{{ isset(Auth::user()->short_name) ? Auth::user()->short_name : '' }}}" 
						name="short_name" 
						placeholder="Sobrenombre">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12 control-label">Carnet</label>
					<div class="col-sm-12">
						<input type="text" class="form-control gray-border" placeholder="{{ Auth::user()->carnet }}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12 control-label">Correo institucional</label>
					<div class="col-sm-12">
						<input type="text" class="form-control gray-border" placeholder="{{ Auth::user()->email }}" disabled>
					</div>
				</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-danger btn-flat pull-right">Guardar</button>
			</div><!-- /.box-footer -->
		</form>
	</div><!-- /.box -->
</div>

<div class="col-lg-6 col-md-12">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Acceso al sistema</h3>
		</div><!-- /.box-header -->

		<div class="col-md-12">
			<p>
				Puedes establecer llaves de acceso alternativas y utilizarlas si es tu preferencia.
				Siempre podras acceder con el sistema centralizado de autenticacion de la USB.
			</p>

			<p>
				Si no estableces estas llaves de acceso, solo podras utilizar tu USBId.
			</p>
		</div>

		<!-- form start -->
		<form class="form-horizontal" method="post" action="{{ url('/users/profile') }}">
			<input type="hidden" value="access-key" name="target">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<div class="box-body">
				<div class="form-group">
					<label for="alt_email" class="col-sm-12 control-label">Correo alternativo</label>
					<div class="col-sm-12">
						<input type="alt_email" 
							class="form-control gray-border" 
							value="{{{ isset(Auth::user()->alt_email) ? Auth::user()->alt_email : '' }}}" 
							name="alt_email" placeholder="Email" required>
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="col-sm-12 control-label">Contraseña</label>
					<div class="col-sm-12">
						<input type="password" class="form-control gray-border" name="password" placeholder="Contraseña" minlength="6" required>
					</div>
				</div>
				<div class="form-group">
					<label for="password_confirm" class="col-sm-12 control-label">Confirmacion</label>
					<div class="col-sm-12">
						<input type="password" class="form-control gray-border" name="password_confirmation" placeholder="Confirmacion de contraseña" minlength="6" required>
					</div>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-info btn-flat pull-right">Guardar</button>
			</div><!-- /.box-footer -->
		</form>
	</div><!-- /.box -->
</div>

<div class="clearfix"></div>
@endsection

