@extends('layouts.blank')

@section('body-classes', 'hold-transition login-page')

@section('body')

<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/') }}" class="display-logo"></a>
  </div><!-- /.login-logo -->
  <div class="login-box-body">
    @if ( isset( $callout ) )
    <div class="callout callout-info">
      <h4>{{ $callout['title'] }}</h4>
      <p>{{ $callout['message'] }}</p>
    </div>
    @endif
    <p class="login-box-msg">Inicia sesion con tus credenciales del sistema</p>
    <form action="{{ url('users/login') }}" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control gray-border" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control gray-border" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-offset-8 col-xs-4">
          <button type="submit" class="btn btn-login btn-block btn-flat">Entrar</button>
        </div><!-- /.col -->
      </div>
      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    </form>

    <div class="social-auth-links text-center">
    	<p>O bien,</p>
    	<a href="{{ url('/users/login/cas') }}" class="btn btn-block btn-social btn-usbid btn-flat">
   			<i class="fa fa-lock"></i> Entrar con tu USBId
    	</a>
    </div><!-- /.social-auth-links -->

    <a href="#">He olvidado mi contraseña</a><br>
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

@endsection