<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Sistema de Transporte Los Teques - San Antonio / @yield('title')</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="/components/components-font-awesome/css/font-awesome.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="/components/AdminLTE/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
				 folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="/components/AdminLTE/css/_all-skins.min.css">

		<link rel="stylesheet" href="/components/datepicker/css/bootstrap-material-datetimepicker.css" />
		


		<link rel="stylesheet" href="/core/styles.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-black-light sidebar-mini">
		<!-- Site wrapper -->
		<div class="wrapper">

			<header class="main-header">
				<!-- Logo -->
				<a href="{{ url('/') }}" class="logo relative-positioned master-logo">
					<div class="logo-container hv-center"></div>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle hidden-md hidden-lg hidden-sm" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li>
								<a href="/" class="hidden-sm hidden-md hidden-lg">Transporte</a>
								<a href="/" class="hidden-xs hidden-sm">Sistema de transporte Los Teques / San Antonio</a>
								<a href="/" class="hidden-xs hidden-lg hidden-md">Transporte LQTS/SAA</a>
							</li>
							<li>
							@if (Auth::guest())
								<a href="{{ url('users/login') }}">
									Acceder
								</a>
						    @else
								<a href="{{ url('users/profile') }}">
									{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->carnet }}}
								</a>
						    @endif
							</li>
							<!-- Control Sidebar Toggle Button -->
							<li>
								<a href="{{ url('users/profile') }}"><i class="fa fa-gears"></i></a>
							</li>
						</ul>
					</div>
				</nav>
			</header>

			<!-- =============================================== -->

			<!-- Left side column. contains the sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					<!-- sidebar menu: : style can be found in sidebar.less -->
					<ul class="sidebar-menu">
						<!--<li class="treeview">
							<a href="#">
								<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
								<li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
							</ul>
						</li>-->
						<li><a href="{{ url('/') }}"><i class="fa fa-bus"></i> <span>Rutas de transporte</span></a></li>
						<li><a href="{{ url('/reports') }}"><i class="fa fa-file-text-o"></i> <span>Reportes</span></a></li>
						<li><a href="{{ url('/notifications') }}"><i class="fa fa-bullhorn"></i> <span>Notificaciones</span></a></li>
						<li><a href="{{ url('/users/profile') }}"><i class="fa fa-user"></i> <span>Mi usuario</span></a></li>
						@if (!Auth::guest())
						@if (Auth::user()->canAdmin())
				            <li class="active treeview">
				              <a href="#!">
				                <i class="fa fa-dashboard"></i>
				                <span> Administracion</span>
				                <i class="fa fa-angle-left pull-right"></i>
				              </a>
				              <ul class="treeview-menu">
				                <li><a href="{{ url('schedule/configure') }}"><i class="fa fa-calendar"></i> Configurar rutas</a></li>
				                <li><a href="{{ url('notifications/configure') }}"><i class="fa fa-bell"></i> Hacer notificacion</a></li>
				                <li><a href="{{ url('reports/configure') }}"><i class="fa fa-newspaper-o"></i> Hacer reportes</a></li>
				                @if (Auth::user()->canSuperAdmin())
				                	<li><a href="{{ url('roles/configure') }}"><i class="fa fa-users"></i> Permisos de usuario</a></li>
				                	<li><a href="{{ url('system/configure') }}"><i class="fa fa-cog"></i> Configuraciones generales</a></li>
				                @endif
				              </ul>
				            </li>
						@endif
						@endif
						<!--<li class="header">LABELS</li>
						<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
						<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
						<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
					</ul>
				</section>
				<!-- /.sidebar -->
			</aside>

			<!-- =============================================== -->

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					@yield('content-header')
				</section>

				<!-- Main content -->
				<section class="content">

					@if ( isset( $callout ) )
					<div class="callout callout-info">
						<h4>{{ $callout['title'] }}</h4>
						<p>{{ $callout['message'] }}</p>
					</div>
					@endif

					@yield('content')

				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->

			<footer class="main-footer">
				Sistema de transporte de Los Teques/San Antonio USB
			</footer>

			<!-- Add the sidebar's background. This div must be placed
					 immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.4 -->
		<script src="/components/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>

		<script src="/components/moment/moment.js"></script>

		<!-- SlimScroll -->
		<script src="/components/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="/components/plugins/fastclick/fastclick.min.js"></script>
		<!-- AdminLTE App -->
		<script src="/components/AdminLTE/js/app.min.js"></script>

		<script type="text/javascript" src="/components/datepicker/js/bootstrap-material-datetimepicker.js"></script>

		@yield('tpl-scripts')
	</body>
</html>
