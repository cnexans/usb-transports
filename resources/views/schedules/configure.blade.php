@extends('layouts.master')

@section('title', 'Home')


@section('content-header')
<h1>
	Configuracion de rutas de transporte
</h1>
@endsection




@section('content')
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Rutas de transporte disponibles
		<a href="{{ url('/schedule/create') }}" class="btn btn-success btn-flat btn-xs spacer-left">
			<i class="fa fa-plus"></i>
			Crear nueva
		</a>
    </h3>
  </div><!-- /.box-header -->
  <div class="box-body">
  	
	    <table class="table table-striped">
	      <tr>
	        <th>Id</th>
	        <th>Ruta</th>
	        <th>Hora de partida</th>
	        <th>Intervalo para anotarse</th>
	        <th>Configuracion</th>
	      </tr>
	      @foreach ($schedules as $schedule)
	      <tr>
	        <td>{{ $schedule->id }}</td>
	        <td>{{ $schedule->name }}</td>
	        <td>{{ $schedule->transformTime($schedule->time) }}</td>
	        <td>{{ $schedule->transformTime($schedule->opens) }} - {{ $schedule->transformTime($schedule->closes) }}</td>
	        <td>
	        	<a href="{{ url('/schedules/'. $schedule->id . '/edit') }}" class="btn btn-info btn-xs pull-left btn-flat">
					<i class="fa fa-edit"></i>
	        		Editar
	        	</a>
	        	<a href="{{ url('/schedules/'. $schedule->id . '/delete') }}" class="btn btn-danger btn-xs pull-left btn-flat spacer-left">
	        		<i class="fa fa-trash-o"></i>
	        		Eliminar
	        	</a>
	        </td>
	      </tr>
	      @endforeach
	    </table>
  	</div><!-- /.box-body -->
</div><!-- /.box -->
<div class="clearfix"></div>
@endsection

