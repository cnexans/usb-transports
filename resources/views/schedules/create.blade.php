@extends('layouts.master')

@section('title', 'Home')


@section('content-header')
<h1>
	Configuracion de rutas de transporte
</h1>
@endsection




@section('content')
<!-- Default box -->
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Modificar ruta</h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<form class="form-horizontal col-lg-6 col-sm-12" method="post" action="{{ url('/schedule/store') }}">
			<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			<div class="box-body">
				<div class="form-group">
					<label for="full_name" class="col-sm-12 control-label">Ruta o destino</label>
					<div class="col-sm-12">
						<input type="text" class="form-control gray-border" 
						name="name" 
						placeholder="Ruta" required>
					</div>
				</div>
				<div class="form-group">
					<label for="full_name" class="col-sm-12 control-label">Hora de partida</label>
					<div class="col-sm-12">
						<input type="text" id="schedule-time" class="form-control gray-border" 
						name="time" 
						placeholder="Hora de partida" required>
					</div>
				</div>
				<div class="form-group">
					<label for="full_name" class="col-sm-12 control-label">Intervalo para anotarse</label>
					<div class="col-sm-6">
						<input type="text" id="schedule-open" class="form-control gray-border" 
						name="opens" 
						placeholder="Abre a las..." required>
					</div>
					<div class="col-sm-6">
						<input type="text" id="schedule-close" class="form-control gray-border" 
						name="closes" 
						placeholder="Cierra a las..." required>
					</div>
				</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-info spacer-left btn-flat pull-right">Guardar</button>
				<a href="{{ url('/schedule/configure') }}" class="btn btn-danger btn-flat pull-right">Cancelar</a>
			</div><!-- /.box-footer -->
		</form>

		<div class="col-lg-6 col-sm-12">
			<p class="spacer-top"><strong>La ruta o destino</strong> corresponde a una pequeña descripción. Algunos autobuses sólo van desde la USB hasta San Antonio, mientras que otros, pasan por San Antonio y llegan a Los Teques. Esta información puede ser de ayuda para los usuarios del transporte.</p>
			<p><strong>La hora de partida</strong> es el momento del día en el cual, el autobús hace la ruta. El formato es de 24 horas.</p>

			<p><strong>El intervalo de tiempo</strong> son las horas en las cuales la lista para la ruta está disponible. Debe asegurarse de que los valores sean consistentes.</p>
		</div>
  	</div><!-- /.box-body -->
</div><!-- /.box -->



<div class="clearfix"></div>
@endsection

@section('tpl-scripts')
<script>
	$('#schedule-time').bootstrapMaterialDatePicker({ 
		date: false, 
		format: 'HH:mm',
		clearButton : false,
		nowButton : false,
		okText : 'Guardar',
		cancelText : 'Cancelar',
		clearText : 'Cerrar'
	});
	$('#schedule-open').bootstrapMaterialDatePicker({ 
		date: false, 
		format: 'HH:mm',
		clearButton : false,
		nowButton : false,
		okText : 'Guardar',
		cancelText : 'Cancelar',
		clearText : 'Cerrar'
	});
	$('#schedule-close').bootstrapMaterialDatePicker({ 
		date: false, 
		format: 'HH:mm',
		clearButton : false,
		nowButton : false,
		okText : 'Guardar',
		cancelText : 'Cancelar',
		clearText : 'Cerrar'
	});
</script>
@endsection