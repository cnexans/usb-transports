@extends('layouts.master')

@section('title', 'Home')


@section('content-header')
<h1>
	Rutas de transporte
</h1>
@endsection




@section('content')
<!-- Default box -->
<div class="box {{ $schedule->available ? 'box-primary' : 'box-danger' }}">
  <div class="box-header with-border">
    <h3 class="box-title">{{ $schedule->name }}, {{ $schedule->time->format('h:i A') }}</h3>
  </div><!-- /.box-header -->
  @if ($schedule->list->count()>0)
  <div class="box-body">
  	
	    <table class="table table-striped">
	      <tr>
	        <th>Carnet</th>
	        <th>Nombre</th>
	      </tr>
	      @foreach ($schedule->list as $user)

	      <tr>
	        <td>{{ $user->carnet }}</td>
	        <td>{{ $user->name }}</td>
	      </tr>
	      @endforeach
	    </table>
  	</div><!-- /.box-body -->
  @else
  	<p>Aun no hay nadie en esta lista.</p>
  @endif
  <div class="box-footer clearfix">
  	@if (!$schedule->available)
    	<p class="center">Esta lista abre entre {{ $schedule->opens->format('h:i A') }} y {{ $schedule->closes->format('h:i A') }}</p>
    @else
		@if (Auth::guest())
			<p class="center">Necesitas estar registrado para anotarte.</p>
	    @else
	    	<a href="{{ url('/schedules/'. $schedule->id . '/write') }}" class="btn btn-info pull-right spacer-left btn-flat">Anotarme</a>
	    @endif

    @endif
  </div>
</div><!-- /.box -->
<div class="clearfix"></div>
@endsection

