<?php

return [



    'titles' => [
        'success' => 'Correcto',
        'error'   => 'Error',
        'denied'  => 'Acceso denegado'
    ],

    'user_profile' => [
        'friendly_names' => [
            'full_name'  => '"nombre completo"',
            'short_name' => '"nombre de pila"'
        ],
        'success'     => 'Se han actualizado los datos de su perfil.',
        'edit_denied' => 'Recientemente ha actualizado sus datos, debe esperar para hacerlo nuevamente.'
    ],

    'user_access_key' => [
        'friendly_names' => [
            'password'  => '"contraseña"',
            'alt_email' => '"email alternativo"'
        ],
        'success' => 'Se han actualizado los datos de su perfil.'
    ],


    'general' => [
        'unknown' => 'No se ha reconocido la peticion'
    ],

    'auth' => [
        'bad_credentials' => [
            'title'   => 'Ups',
            'message' => 'Las credenciales introducidas son invalidas'
        ]
    ],

    'list' => [
        'ready' => [
            'title'   => '',
            'message' => 'Genial, ya estas anotado'
        ]
    ],

    'admin' => [
        'schedules-update' => [
            'title'   => 'Exito',
            'message' => 'La configuracion ha sido guardada'
        ]
    ],

    'schedule' => [
        'key' => [
            'name' => 'ruta o destino',
            'opens' => 'hora de apertura',
            'closes' => 'hora de cierre',
            'time' => 'hora de partida'
        ]
    ]
    

];